# *  Librerator 2 -- A library management program.  ---------------------------|
#    Copyright (C) 2019  M E Leypold
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# *  Env.sh: Detecting libr path and setting environment variables  -----------|

# if test "bash" != "$0"; then
#    echo >&2 "libr2/Env.sh: Must be sourced by bash, got: $0"
# else
    __libr2_activate(){
	local MYPATH="${BASH_SOURCE[0]}"       
	case "$MYPATH" in
	    /*)  MYDIR="$(dirname "$MYPATH")";;
	    */*) MYDIR="$(realpath "$(dirname "$MYPATH")")";;
	    *)   MYDIR="$PWD";;
	esac

	if ! test -d "$MYDIR/.pyenv3"; then
	( set -eu
          cd "$MYDIR"
	  ./Setup.sh
	)
	fi
	PATH="$MYDIR:$PATH"
	. "$MYDIR/.pyenv3/bin/activate"
    }
    
    __libr2_activate
    unset -f __libr2_activate
# fi
