# *  Librerator 2 -- A library management program.  ---------------------------|
# *  Copyright (C) 2019  M E Leypold
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# *  libr2.featureflags: Feature Flags  ---------------------------------------|

import yaml

# ** Testing for feature flags  -----------------------------------------------|

class FeatureNotImplemented(Exception):
    pass

class UnknownFeature(Exception):
    pass

# ** Feature flags registry  --------------------------------------------------|

class Assert(object):
    def __init__(self, registry):
        self.registry = registry

    def __getattr__(self,name):
        registry = self.__dict__['registry']
        registry.__assert_inactive__(name)

class FeatureRegistry(object):
    def __init__(self, **flags):
        self.flags = flags

    @property
    def assert_inactive(self):
        return Assert(self)

    @property
    def not_implemented(self):
        return Assert(self)

    def __assert_inactive__(self,name):
        if not name in self.flags:
            raise UnKnownFeature(name)
        if self.flags[name]:
            raise FeatureNotImplemented(name)

    def __getattr__(self,name):
        if name in self.__dict__:
            return self.__dict__[name]
        flags = self.__dict__['flags']
        if name not in flags:
            raise UnKnownFeature(name)
        return flags[name]

# ** Hard coded configuration  ------------------------------------------------|

feature = FeatureRegistry (
)
