# *  Librerator 2 -- A library management program.  ---------------------------|
#    Copyright (C) 2019  M E Leypold
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# *  libr2.pdfs.py: Tools for handling PDF books ------------------------------|

import sys
import os
import PyPDF2
import isbnlib
import copy
import pdftotext
import yaml

class Info(object):
    def __init__(self, attributes, **more):
        self.__attributes__ = {}
        
        for key, value in attributes.items():
            self.set_attr(key, value)

        for key, value in more.items():
            self.set_attr(key, value)            
            
    Literals = {
        '/False' : False,
        '/True'  : True,        
    }

    def convert(self, value, hint):
        if value in self.Literals:
            return self.Literals[value]
        else:
            return value

        # we could do more conversion here
    
            
    def set_attr(self,name,value):
        name = str(name)
        assert len(name)>0
        if name[0] in "/":
            name = name[1:]
        name  = name.lower()
        value = self.convert(value, name)
        
        setattr(self,name,value)
        self.__attributes__[name] = value

    def __setitem__(self,key,value):
        self.set_attr(key,value)
        
    def __getitem__(self,key):
        return self.__attributes__[key]
        
    @property
    def as_dict(self):
        return copy.deepcopy(self.__attributes__)

class PdfFile(object):

    # TODO: It would be very useful to have some kind of caching
    # available, so we would not extract the same information over and
    # over again from a read-only file.
    #
    # use inode, size, date or sha256as keys. Cache in ~/.libr2 as shelf(s)
    #
    # Also: Use a page object cache internally
    
    def __init__(self, filepath):
        self.filepath        = filepath
        self.__pages__       = None
        self.__text_pages__  = None
        self.__text__        = None

        with open(self.filepath, 'rb') as f:
            document           = PyPDF2.PdfFileReader(f)
            self.n_pages       = document.getNumPages()
            self.document_info = document.getDocumentInfo()
        
        # Note: I'm not using the PyPDF2 pages currently, because:
        #
        # - This is much slower than the pdftotext method to extract text
        # - somehow doesn't contain the links as text (and I want to match on those)

    # TODO: rewrite the pages stuff to use pdftotext, remove methods not needed

    @property
    def info(self):
        return self.get_info()

    def get_info(self,**more):
        return Info(self.document_info, page_count = self.n_pages, **more)
        
    
    def get_pages(self):
        assert self.file is not None    
        for page in self.document.pages:
            yield page

    @property
    def pages(self):
        if self.__pages__ is None:
            self.__pages__ = list(self.get_pages())
        return self.__pages__

    def get_text_pages(self):
        # use self.__text_pages__ if available, so repeated use doesn't cost so much
        return (page.extractText().split("\n") for page in self.pages)

    @property
    def text_pages(self):
        if self.__text_pages__ is None:
            self.__text_pages__ = list(self.get_text_pages())
        return self.__text_pages__

    # TODO: Cleanup-Function/Method to remove watermark from ascii text
    
    def get_lines_by_pypdf(self):
        # XXX use __text_pages__ if available
        for page in self.get_text_pages():
            for line in page:
                yield line

    def get_lines(self):
        with open(self.filepath, "rb") as f:
            pdf = pdftotext.PDF(f)
            for page in pdf:
                for line in page.split("\n"):                    
                    yield(line)
                
    def get_isbns(self):
        isbn10s = []
        for line in self.get_lines():
            for isbn in isbnlib.get_isbnlike(line.replace("-","")):
                if isbnlib.is_isbn13(isbn):
                    yield isbnlib.get_canonical_isbn(isbn)
                if isbnlib.is_isbn10(isbn):
                    isbn10s.append(isbnlib.get_canonical_isbn(isbn))
        for isbn in isbn10s:
            yield isbn
                    
    @property
    def text(self):
        if self.__text__ is None:
            self.__text__ = list(self.get_lines())
        return self.__text__
            
    def load_all(self):
        self.pages
        self.text_pages
        self.text
        
    def __enter__(self):
        return self
        
    def __exit__(self, excn_type, excn_value, traceback):
        pass

    # Context handler


class NoFallbackDb(Exception):
    pass

class FallbackDb(object):

    # Currently the fallback db is organized along "publisher ids"
    # (which might be ISBNs), but the only publisher we support is the
    # pragmatic bookshelf. Especially the loading will have to be
    # extended when we want to support more publishers with different
    # publisher id schemes.

    instance = None    

    def __init__(self):
        self.dbpath = os.getenv("LIBR_FALLBACK_DB")
        if self.dbpath is None:
            raise NoFallbackDb("Environement vairable not set: LIBR_FALLBACK_DB")
        with open(self.dbpath) as f:
            data = yaml.load(f)

        # TODO: Add safe default loader: https://msg.pyyaml.org/load
            
        self.records           = []
        self.by_isbn           = {}
        self.by_publisher_id   = {}
        self.by_filename_stem  = {}
        
        for publisher_id, values in data.items():
            publisher_id = str(publisher_id)
            if not 'stem' in values:
                values['stem'] = None
                stem = None
            record = Info(values, publisher_id = publisher_id )        
            self.records.append(record)
            self.by_publisher_id[publisher_id] = record
            if record.isbn is not None:
                record.set_attr('isbn', str(record.isbn))
                self.by_isbn[record.isbn] = record
            if record.stem is not None:
                self.by_filename_stem[record.stem] = record
    
    def get_info_for_isbn(self, isbn):
        if isbn in self.by_isbn:
            return self.by_isbn[isbn]
        return None

    def get_info_for_publisher_id(self, publisher_id):
        if publisher_id in self.by_publisher_id:
            return self.by_publisher_id[publisher_id]
        return None

    def get_info_for_filename_stem(self, filename_stem):
        if filename_stem in self.by_filename_stem:
            return self.by_filename_stem[filename_stem]
        return None    
    
    @classmethod
    def get(cls):
        if cls.instance is None:
            cls.instance = cls()
        
        return cls.instance
