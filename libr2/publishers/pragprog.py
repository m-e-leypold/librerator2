# *  Librerator 2 -- A library management program.  ---------------------------|
#    Copyright (C) 2019  M E Leypold
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# *  libr2.publisher.pragprog: Handling books from the Pragmatic Bookshelf  ---|

import re
import os
from ..pdfs import PdfFile, FallbackDb

class RX(object):

    rxs = {
        'this_books_homepage' : "^( *This *Book’s *Home Page|Home Page for |.*s *Home *Page$)",
        'code_url'            : ".*https{0,1}://[^ \t]*/([^/]*)/(code|source_code|source)(/| )",
        'book_version'        : "^Book *version: *(.*) *— *(.*)",
        'book_version_2'      : "^Version: *([^,]*), *(.*)",
        'book_version_3'      : "^ISBN-13: *[-0-9]+ *([^ ]+) *printing, *(.*) +Version:"

        # version_3
        # 
    }

    def __init__(self):
        for key, rx in self.rxs.items():
            setattr(self, key, re.compile(rx))

rx = RX()
    

class IdNotFound(Exception):
    pass

class IsbnNotFound(Exception):
    pass

class PrintingNotFound(Exception):
    pass

class PdfBook(PdfFile):

    @property
    def isbn(self):
        try:
            return next(self.get_isbns())
        except StopIteration:
            isbn = FallbackDb.get().get_info_for_filename_stem(self.filename_stem).isbn
            if isbn is not None:
                return isbn
        raise IsbnNotFound(self.filepath)
    
    def get_info(self,**more):
        printing, date = self.get_printing()
        return super().get_info(
            isbn   = self.isbn,
            pbs_id = self.get_pbsid(),
            printing = printing,
            printing_date = date,
            **more
        )

    # TODO: Add booksite https://pragprog.com/titles/$PBSID
    
    def get_pbsid(self):

        lines = self.get_lines()

        def next_line():
            try:
                line = next(lines)
                return line
            except StopIteration:
                return None

        while True:
            line = next_line()
            if line is None:
                break
            
            match = rx.this_books_homepage.match(line)
            if match:
                line = next_line()
                if line is None:
                    break
                return line.split('/')[-1]

        # It should be relatively rare that we arrive here. For this
        # reason, we're not matching both rxs in the same loop, since
        # we want this_books_homepage to have precendence. In the rare
        # case that this doesn't ever match, we have to do the extra
        # work of iterating over the (newly generated) lines again-
            
        for line in self.get_lines():
            match = rx.code_url.match(line)
            if match:
                return match.group(1)

        isbn = self.isbn
        info = FallbackDb.get().get_info_for_isbn(isbn)
        if info is not None:
            return info.publisher_id # This is the pbsid for pragprog books
    
        raise IdNotFound(self.filepath)

    @property
    def filename_stem_and_version(self):
        filename = os.path.basename(self.filepath)
        i        = filename.rindex(".")
        suffix   = filename[i:]
        stem     = filename[:i]

        if suffix in (".pdf",".PDF"):
            try:
                k    = filename.rindex("_")
            except ValueError:
                return stem,None

            version = stem[k+1:]


            if len(version)>0 and version[0] in ('P','B'): # XXX check by regex
                return stem[:k],version

            return stem, None  # The version was not a plausible version string
            
        assert False, "Unknown suffix in for pdf file, better to bail out"

    @property
    def filename_stem(self):
        return self.filename_stem_and_version[0]
        
    def get_printing(self):
        r1 = rx.book_version
        r2 = rx.book_version_2
        r3 = rx.book_version_3
        
        for line in self.get_lines():
            for r in (r1, r2, r3):
                match = r.match(line)
                if match:
                    return (match.group(1),match.group(2))

        filename = os.path.basename(self.filepath)
        i        = filename.rindex(".")
        suffix   = filename[i:]
        stem     = filename[:i]

        if suffix in (".pdf",".PDF"):
            components = stem.split("_")
            if len(components) >= 2:
                version = components[-1]
                if len(version)>0 and version[0] in ("B","P"):
                    return (version, None)
                
        raise PrintingNotFound(self.filepath)


    # booksite must be matched as URL, but with a specific context
    #
    # (which seems to be different from book to book)
    #
    # - Urls ending on /source_code or /code are pretty good guess
    # - e.g. for purchase at our store: https://pragprog.com/book/cmelixir
    # - This Book’s Home Page\nhttps://pragprog.com/book/ppmetr2\nSource code from this book, errata, and other resources.
    #
    # These might (or might not) covery everything
