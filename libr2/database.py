import os
from os import path

from sqlalchemy import Table, Column, Integer, Numeric, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from .objects import LibrObject


class DB (object):
    MappedObject = declarative_base()

    @property
    def metadata(self):
        return self.MappedObject.metadata

    def __init__(self, db_dir, force_init = False):
        self.dir  = db_dir
        self.path = path.join(db_dir, "db.squlite3")
        try:
            os.remove(self.path)
        except FileNotFoundError:
            pass

        os.makedirs(self.dir, exist_ok=True)

        engine = self.engine = create_engine('sqlite:///DB/db.squlite3')
        self.metadata.create_all(engine)

        self.new_session = sessionmaker(bind=engine)

    @property
    def session(self):
        return Session(self.new_session())


class Session(object):

    def __init__(self, db_session):
        self.db_session = db_session

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):

        # TBD: Should commit/fail on __exit__

        pass

    def commit(self):
        self.db_session.commit()

    def flush(self):
        self.db_session.flush()

    def add(self, obj):
        self.db_session.add(obj)


class Book(DB.MappedObject):
    __tablename__ = 'books'

    id        = Column(String(), primary_key=True)
    title     = Column(String())
    subtitle  = Column(String())
    authors   = Column(String())
    isbn      = Column(String())
    publisher = Column(String())
    edition   = Column(String())

    # TBD: Change Edition to bigint
    # TBD: description
    # TBD: envelope (line after '*')

    @classmethod
    def load(cls, session, libr_object):
        obj = cls(
            id        = libr_object.id,
            title     = libr_object.title,
            subtitle  = libr_object.subtitle,
            authors   = libr_object.authors,
            isbn      = libr_object.isbn,
            publisher = libr_object.publisher,
            edition   = libr_object.edition,

        )
        session.add(obj)
        return obj

    # TBD: Normalize ISBN
    # TBD: Normalize Edition (maybe do this when reading LibrObject)

    @classmethod
    def bulk_load(cls, session, libr_objects):
        objects = [cls.load(session, libr_object) for libr_object in libr_objects]
        session.flush()
        return objects
