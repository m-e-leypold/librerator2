# *  Librerator 2 -- A library management program.  ---------------------------|
# *  Copyright (C) 2019  M E Leypold
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# *  libr2.objects: Filesystem objects ----------------------------------------|

from collections import deque
from os import listdir, path
from .tools import index, read_text, Window, optional, NotFound, re_compile


class rx(object):
    table_line     = "[|]"
    non_table_line = "[^|]|$"
    empty_line     = "[ \t]*$"
    non_empty_line = ".*[^ \t].*$"
    local_vars     = "[*]  *#"
    data_line      = "[|] *[^ ]+ *[|]"


class CheckError(Exception):
    pass


class LibrObject(object):

    required_attributes = ['id', 'title']
    nullable_attributes = ['authors', 'subtitle', 'publisher', 'year', 'edition', 'isbn']

    def __init__(self, obj_path):
        self.path        = obj_path
        self.parse_description()

    def __load_attributes__(self, a_dict, *attrs):

        for attr in attrs:
            if attr in a_dict:
                value = a_dict[attr]
                if value == "":
                    value = None
                setattr(self, attr, a_dict[attr])
            else:
                setattr(self, attr, None)

    def __check_attributes__(self):
        for attr in self.required_attributes:
            if not hasattr(self, attr) or not getattr(self, attr) is not None:
                self.parse_error = CheckError("missing attribute", attr)
                return

        if self.id != path.basename(self.path):
            self.parse_error = CheckError("id and path differ", self.id, self.path)
            return

    def parse_description(self):

        # TBD: parse_errors should be a list (so multiple check errors can be dumped/printed)

        self.parse_error = None

        try:
            filepath = path.join(self.path, "Description")
            file     = Window.from_file(filepath)

            # TBD: Do some logging via the python logger here

            preamble, table, _ws, description, _local_vars = file.split(
                rx.table_line, rx.non_table_line, rx.non_empty_line, optional(rx.local_vars)
            )

        except NotFound as err:
            self.parse_error = err
        except UnicodeDecodeError as err:
            self.parse_error = err

        if self.parse_error is not None:
            return

        if len(table) < len(self.required_attributes):
            self.parse_error = CheckError("table too short", len(table))
            return

        dataline_rx = re_compile(rx.data_line)

        def is_a_dataline(line):
            return dataline_rx.match(line) is not None

        def get_key(line):
            return line.split("|")[1].strip()

        def get_value(line):
            return line.split("|")[2].strip()

        values = table.get_key_values(is_a_dataline, get_key, get_value)

        self.__load_attributes__(
            values,
            *(self.required_attributes + self.nullable_attributes)
        )

        self.__check_attributes__()

        # TBD: Check path basename vs extracted ID

    def __repr__(self):
        return "<{CLASS} path={PATH!r}>".format(
            CLASS = self.__class__.__name__,
            PATH  = self.path
        )

    # TBD: Get Effective time stamp (description + dir)

    @classmethod
    def ignore(cls, dirname, basename):
        return basename in (".git", "CVS", "INCOMING", "DUPLICATES", "software")

    @classmethod
    def find_dirs(cls, root):
        paths = deque([root])
        while len(paths) > 0:
            p = paths.popleft()
            d = path.join(p, "Description")
            if path.exists(d) and path.isfile(d):
                yield(p)
                continue
            else:
                for name in listdir(p):
                    if not cls.ignore(p, name):
                        child = path.join(p, name)
                        if path.isdir(child) and not path.islink(child):
                            paths.append(child)

    @classmethod
    def get_dirs(cls, root):
        return index(cls.find_dirs(root), get_key = (lambda p: path.basename(p)))

    @classmethod
    def get_objects(cls, path):
        return {key: cls(path) for key, path in cls.get_dirs(path).items()}
