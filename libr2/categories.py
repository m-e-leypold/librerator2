
import os
import yaml
from .featureflags import feature


class Node(object):

    def __init__(self, name, parent):

        self.description = None
        self.aliased_to  = None
        self.name        = name
        self.parent      = parent
        self.children    = None

        if parent is None:
            assert self.name is None
            self.fullname = None
        else:
            assert self.name is not None
            if parent.fullname is None:
                self.fullname = name
            else:
                self.fullname = parent.fullname + "." + name

    @property
    def is_alias(self):
        return self.aliased_to is not None and self.children is None

    @property
    def is_topic(self):
        return self.aliased_to is None and self.children is None

    @property
    def is_inner(self):
        return self.children is not None


    def __repr__(self):
        return "<# {CLASS} {ID}>".format(
            CLASS = self.__class__.__name__,
            ID    = self.fullname
        )

    def __str__(self):
        return self.fullname if self.fullname is not None else "---"


    @classmethod
    def create(cls, name, parent, **kwargs):
        if 'Category' in kwargs:
            return Category(name, parent, **kwargs)
        elif 'See' in kwargs:
            return Alias(name, parent, **kwargs)
        else:
            return Topic(name, parent, **kwargs)

    def get_leafs(self, result):
        if self.children is None:
            result.append(self)
            return
        for _, child in self.children.items():
            child.get_leafs(result)
        return result

class Tree(Node):

    def __init__(self, name, parent, **data):
        super().__init__(name, parent)
        self.children = { key: Node.create(key, self, **value)
                          for key, value in data.items()
                          if not (type(key) == str and key[0].isupper())
        }

    @classmethod
    def create_root(cls, **data):
        return Tree(None,None,**data)

class Category(Tree):

    def __init__(self,name, parent, Category,Note=None,**more):
        super().__init__(name, parent, **more)
        self.description = Category

class Topic(Node):

    def __init__(self, name, parent, Topic, Directory, Note = None, **more):
        super().__init__(name, parent)
        self.description = Topic

class Alias(Node):

    def __init__(self, name, parent, See, Note = None, **more):
        super().__init__(name, parent)
        self.aliased_to = See


__categories__ = None

def parse_categories():
    global __categories__
    if __categories__ is None:
        filestem, ext = os.path.splitext(__file__)
        filepath = filestem + ".yaml"
        with open(filepath) as f:
            data = yaml.load(f, Loader = yaml.FullLoader)
        __categories__ = Tree.create_root(**data)
        return __categories__

def get_topics():
    result = []
    parse_categories().get_leafs(result)
    return result

