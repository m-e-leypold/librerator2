# *  Librerator 2 -- A library management program.  ---------------------------|
# *  Copyright (C) 2019  M E Leypold
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# *  libr2.tools: Various Tools  ----------------------------------------------|

import re

from .runtime import Panic

from dataclasses import dataclass
from typing import List, Any
from collections import deque

# ** Indexing Data  -----------------------------------------------------------|


class DuplicateKey(Panic):

    message = "duplicate key"


def index(items, get_key, convert=(lambda x: x)):
    d = {}
    for item in items:
        key = get_key(item)
        if key in d:
            raise DuplicateKey(key, item, d[key])
        d[key] = convert(item)
    return d

# ** Reading Files  -----------------------------------------------------------|


def read_text(path):
    with open(path) as f:
        return [line.rstrip() for line in f.readlines()]

# ** Windows Based Parsing ----------------------------------------------------|
# *** Utilities  --------------------------------------------------------------|


class NotFound(Exception):
    # TBD: The error reporting could be more detailed, but this
    # requires that we somehow recover the information which regex
    # failed.
    #
    pass


def format_output(file, **data):
    print(template.format(**data), file=file)


def first(predicate, items, start = 0, end = None, error = True):

    if end is None:
        end = len(items)
    try:
        pos = next((i for i, item in enumerate(items[start:end]) if predicate(item)))
    except StopIteration:
        if error:
            raise NotFound("item satisfying {PREDICATE} not found.".format(PREDICATE=str(predicate)))
        else:
            return None
    return start + pos


def last(predicate, items, start = 0, end = None, error = True):
    if end is None:
        end = len(items)
    try:
        return start + next((i for i, item in reversed(list(enumerate(items[start:end]))) if predicate(item)))
    except StopIteration:
        if error:
            raise NotFound("item satisfying {PREDICATE} not found.".format(PREDICATE=str(predicate)))
        else:
            return None


compiled_rxs = {}


def re_compile(rx):
    try:
        compiled = compiled_rxs[rx]
    except KeyError:
        compiled = re.compile(rx)
        compiled_rxs[rx] = compiled
    return compiled


def matches(rx):
    if type(rx) == str:
        compiled = re_compile(rx)
    else:
        compiled = rx
        assert type(rx) == re.Pattern

    def matches(s):
        return compiled.match(s)

    return matches


def matches_not(rx):
    if type(rx) == str:
        compiled = re_compile(rx)
    else:
        compiled = rx
        assert type(rx) == re.Pattern

    def matches(s):
        m = compiled.match(s)
        return (m is None)
    return matches


def indented(s):
    return len(s) > 0 and s[0] in [' ', '\t']


def not_indented(s):
    return len(s) > 0 and s[0] not in [' ', '\t']


def empty(s):
    return len(s) is None


# *** Text windows (the best thing since sliced bread)  -----------------------|

@dataclass
class optional:
    predicate: Any

    # XXX static unpack procedure

@dataclass
class Window:

    text  : List[str]
    start : int
    end   : int

    def to_real_predicate(self, predicate):
        if type(predicate) == str or type(predicate) == re.Pattern:
            return matches(predicate)
        return predicate

    def first(self, predicate, is_optional):
        return first(predicate, self.text, self.start, self.end, not is_optional)

    def __split__(self, predicates, windows):
        is_optional = False
        if len(predicates) == 0:
            windows.append(self)
            return

        skipped = 0

        tail = None
        while tail is None:
            if len(predicates) == 0:
                break
            p   = predicates.popleft()
            if type(p) == optional:
                p           = p.predicate
                is_optional = True
            p   = self.to_real_predicate(p)
            pos = self.first(p, is_optional)
            if pos is not None:
                windows.append(Window(self.text, self.start, pos))
                tail = Window(self.text, pos, self.end)
            else:
                skipped = skipped + 1

        for i in range(0, skipped):
            windows.append(None)

        if tail is not None:
            tail.__split__(predicates, windows)
        else:
            windows.append(None)  # TBD: Why is this necessary?

    def split(self, *_predicates):

        predicates = deque(_predicates)
        windows    = deque()
        self.__split__(predicates, windows)

        assert len(windows) == len(_predicates) + 1
        return list(windows)

    def __repr__(self):
        return "Window(<{CLASS} id={ID}>, {START}, {END})" \
            .format(
                CLASS = type(self.text).__name__,
                ID    = id(self.text),
                START = self.start,
                END   = self.end
            )

    def __str__(self):
        return "\n".join((
            "{LINE:4d} {TEXT}".format(TEXT=text, LINE=line + self.start)
            for line, text in enumerate(self.text[self.start:self.end])
        ))

    def __len__(self):
        return self.end - self.start

    def __getitem__(self, i):
        if i < 0:
            i = i + (self.end - self.start)
        assert i >= 0
        i = i + self.start
        assert i < self.end
        return self.text[i]

    def to_line(self):
        return " ".join(self.text[self.start:self.end])

    def lines(self):
        return self.text[self.start:self.end]

    def get_items(self, matching, convert):
        return [convert(line) for line in self.text[self.start:self.end] if matching(line)]

    def get_key_values(self, matching, get_key, get_value):
        return {get_key(line): get_value(line) for line in self.text[self.start:self.end] if matching(line)}

    @classmethod
    def from_file(cls, path):
        text = read_text(path)
        return cls(text, 0, len(text))
