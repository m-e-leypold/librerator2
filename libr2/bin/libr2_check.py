# *  Librerator 2 -- A library management program.  ---------------------------|
# *  Copyright (C) 2019  M E Leypold
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#
# *  libr2.bin.libr2_check: Built-In Tests ------------------------------------|
#
# ** Global Usage  ------------------------------------------------------------|
#
#      See doc strings of sub-command below for usage of specific sub-commands.

"""Librerator 2 Built-In Tests.

Usage:
  libr-check [--help] [--warranty] <command> [<args>...]

Options:
  -h --help      Show this screen.
  -w --warranty  Show warranty.

The most commonly used commands are:
   pep8  run PEP-8 checks on the libr2 sources

See 'libr help <command>' for more information on a specific command or help topic.
See 'libr help commands' for a complete list of subcommands.
"""

# TBD: add topical help pages (from various modules)

# ** Global Data   ------------------------------------------------------------|

version    = "libr 0.0.0"

import sys
from   docopt import docopt
from   ..runtime import Panic

# ** Sub-Command Registry  ----------------------------------------------------|

# TBD: Use same registry in libr2 and libr2_check

class command(object):

    registry = {}
        
    def __init__(self, name, help=None):
        self.name = name
        self.help = help
        
    def __call__(self, proc):
        self.proc = proc
        self.registry[self.name] = self
        return proc

    def run(self,gargs,args):    
        cmd_args = docopt(self.proc.__doc__, argv=[self.name]+args)
        del cmd_args[self.name]
        self.proc(gargs,cmd_args)
        
    @classmethod
    def dispatch(cls, name, gargs, args):

        # TBD: Here we could process the global arguments

        if name == "help":
            if len(args) == 0:
                docopt(__doc__, version=version, argv=['--help'])
            else:
                # TBD: here we can process the additional help pages, if any are available
                name = args[0]
                args=["--help"]
                if name == "commands":
                    print()
                    for cmd,obj in sorted(cls.registry.items()):
                        if obj.help == None:                            
                            print("{CMD:15} - {HELP}".format(CMD=cmd,HELP="[undocumented]"))
                        else:
                            print("{CMD:15} - {HELP}".format(CMD=cmd,HELP=obj.help))
                    print()                                           
                    exit(0)

                
        if name not in cls.registry:
            exit("'{NAME}' is not a libr command. See 'libr help'.".format(NAME=name))
        
        cls.registry[name].run(gargs,args)

# ** Subcommand 'pep8' --------------------------------------------------------|

pep8_ignores = {
    "E501":  """line too long (80 > 79 characters)
    Yes this is a problem, mostly comes from the oushin headers.
    Only I don't want to fix this right now.
    """,
    
    "E221" : """ multiple spaces before operator.
    I want to be able to align assignments where it
    makes senses to understand a series of aligments as a
    table.
    """,
    
    "E251" : """ unexpected spaces around keyword / parameter equals,
    I think it increases readability if one can write
    
      foo(f, Loader = very(long.expression))
    """,

    "E203" : """whitespace before ':'.
    I definitely want to be able to align ':' for readability.
    """,
}

@command("pep8", "Run PEP-8 checks on the libr2 module tree")
def pep8(gargs,args):
    """
    Sub-command 'pep8'.

    Usage:
      libr pep8

    TBD: Description
    """

    import os
    import subprocess
    import libr2
    
    root = os.path.dirname(os.path.dirname(libr2.__file__))
    os.chdir(root)

    cmdline = ["pycodestyle"] + [ "--ignore="+",".join(pep8_ignores) ]+ ["libr2"] 

    print("running:", " ".join(cmdline))
    subprocess.run(cmdline, check=True)
    
    # TBD: Split in procedure returning success/fail and cli

# ** main()  ------------------------------------------------------------------|

def main(name=sys.argv[0], argv=sys.argv[1:]):
    gargs    = docopt(__doc__, version=version, options_first=True, argv=argv)
    cmd      = gargs['<command>']
    cmd_args = gargs['<args>']

    del gargs['<command>']
    del gargs['<args>']

    try:
        command.dispatch(cmd,gargs,cmd_args)
    except Panic as p:
        from os import path
        print(path.basename(name)+": *** Error ***", p, file=sys.stderr)

    
