# *  Librerator 2 -- A library management program.  ---------------------------|
# *  Copyright (C) 2019  M E Leypold
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#
# *  libr2.bin.libr2: The Command Line Interface  -----------------------------|
#
# ** Global Usage  ------------------------------------------------------------|
#
#      See doc strings of sub-command below for usage of specific sub-commands.

"""Librerator 2 -- a library manager.

Usage:
  libr [--version] [--help] [--verbose] <command> [<args>...]

Options:
  -h --help      Show this screen.
  -V --version   Show version.
  -w --warranty  Show warranty.
 
  -c --verbose   Be verbose.

The most commonly used commands are:
   find-objects  Find the data objects in file tree.

See 'libr help <command>' for more information on a specific command or help topic.
See 'libr help commands' for a complete list of subcommands.
"""

# TBD: add topical help pages (from various modules)

# ** Global Data   ------------------------------------------------------------|

version    = "libr 0.0.0"

import sys
from   docopt import docopt
from   ..runtime import Panic

# ** Sub-Command Registry  ----------------------------------------------------|

class command(object):

    # Static class data is the command registry. Instances represent
    # single commands.
    
    registry = {}
    command_column_width = 10
    
    def __init__(self, name, help=None):
        self.name = name
        self.help = help
        if len(name)>self.command_column_width:
            self.__class__.command_column_width = len(name)
            
        
    def __call__(self, proc):
        self.proc = proc
        self.registry[self.name] = self
        return proc

    def run(self,gargs,args):    
        cmd_args = docopt(self.proc.__doc__, argv=[self.name]+args)
        del cmd_args[self.name]
        self.proc(gargs,cmd_args)
        
    @classmethod
    def dispatch(cls, name, gargs, args):

        # TBD: Here we could process the global arguments

        if name == "help":
            if len(args) == 0:
                docopt(__doc__, version=version, argv=['--help'])
            else:
                # TBD: here we can process the additional help pages, if any are available
                name = args[0]
                args=["--help"]
                if name == "commands":
                    print()
                    for cmd,obj in sorted(cls.registry.items()):
                        if obj.help == None:                            
                            print("{CMD:{WIDTH}} - {HELP}".format(
                                CMD   = cmd,
                                HELP  = "[undocumented]",
                                WIDTH = cls.command_column_width))
                        else:
                            print("{CMD:{WIDTH}} - {HELP}".format(
                                CMD   = cmd,
                                HELP  = obj.help,
                                WIDTH = cls.command_column_width))
                    print()                                           
                    exit(0)

                
        if name not in cls.registry:
            exit("'{NAME}' is not a libr command or help page. See 'libr help'.".format(NAME=name))
        
        cls.registry[name].run(gargs,args)

# ** Sub-Command 'find-objects'  ----------------------------------------------|
        
@command("find-objects", "find library objects in file tree.")
def find_objects(gargs,args):
    """
    Sub-command 'libr find-objects':

    Usage:
      libr find-objects [<path>]

    Find all library objects in tree <path>. If <path> is not given,
    the current working directory '.' is used instead.

    The found objects will be output as a sorted tap separated list of 
    object id (per convention the basename of the object path) and the path to the 
    directory which represents the object. 

    In example:

       whitaker+evans+voth_chained-exploits  informit/whitaker+evans+voth_chained-exploits
       venkat-subramaniam_pragmatic-scala    pragprog/venkat-subramaniam_pragmatic-scala
    """

    from ..objects import LibrObject

    root = args['<path>']
    if root == None:
        root="."

    from os import path

    paths = LibrObject.get_dirs(root)

    for k,p in sorted(paths.items()):
        print(k,p,sep="\t")

# ** Sub-Command 'parse-objects'  ---------------------------------------------|

@command("parse-objects", "find and parse library objects in file tree.")
def parse_objects(gargs,args):
    """
    Sub-command 'libr parse-objects':

    Usage:
      libr parse-objects [<path>]

    Find and parse all library objects in tree <path>. If <path> is
    not given, the current working directory '.' is used instead.

    (TBD)
    """

    from ..objects import LibrObject

    root = args['<path>']
    if root == None:
        root="."

    from os import path

    objects = LibrObject.get_objects(root)

    # TBD: factor error reporting out, share with load-objects
    
    errors  = [ obj for _,obj in objects.items() if obj.parse_error != None ]

    if len(errors) > 0:
        print("Errors occured while parsing objects:",file=sys.stderr)
        for obj in errors:
            print("=> Error: In",obj.path+":",obj.parse_error,file=sys.stderr)
        raise Panic("Parse errors.")
            
    for k,obj in sorted(objects.items()):
        print(k,obj,sep="\t")            # TBD: Improve on this


# ** Sub-Command 'parse-object'  ----------------------------------------------|

@command("parse-object", "find and parse library objects in file tree.")
def parse_objects(gargs,args):
    """
    Sub-command 'libr parse-object':

    Usage:
      libr parse-object <path>

    parse the single object located at path.

    (TBD)
    """

    from ..objects import LibrObject

    obj_path = args['<path>']

    from os import path

    obj = LibrObject(obj_path)

    print(obj)

# ** Sub-Command 'parse-categories'  ----------------------------------------------|

@command("parse-categories", "parse the categories definition file")
def parse_categories(gargs,args):
    """
    Sub-command 'libr parse-categories':

    Usage:
      libr parse-categories

    (TBD)
    """

    from ..categories import parse_categories, get_topics
    for topic in get_topics():
        if   topic.is_alias:
            print("A {FULLNAME:40} => {ALIASED}".format(
                FULLNAME = topic.fullname,
                ALIASED  = topic.aliased_to
            ))
        elif topic.is_topic:
            print("T {FULLNAME:40} {DESCRIPTION}".format(
                FULLNAME    = topic.fullname,
                DESCRIPTION = topic.description
            ))
            pass
        else:
            assert False, "Should only have gotten Topic and Alias nodes from get_topics()"

    
# ** Sub-Command 'create-db'  -------------------------------------------------|

@command("create-db", "create the data base")
def db_create(gargs,args):
    """
    Sub-command 'libr create-db':

    Usage:
      libr create-db [<dir>]

    (TBD) Default dir is DB/db.sqlite3
    """

    # TBD: Make this dependent on a "force" parameter, pass db path as a global option
    
    import os
    from os import path
    
    db_dir = args['<dir>']
    if db_dir == None:
        db_dir="DB"

    from ..database import DB

    db = DB(db_dir, force_init = True)

# ** Sub-Command 'load-objects'  ----------------------------------------------|

@command("load-objects", "Load filesystem objects into database")
def load_objects(gargs,args):
    """
    Sub-command 'libr load-objects':

    Usage:
      libr load-objects [<path>] [<dir>]

    The following global obtions apply:
    
      (TBD)
    """

    import os
    from os import path
    
    db_dir = args['<dir>']
    if db_dir == None:
        db_dir="DB"
    
    from ..objects import LibrObject

    root = args['<path>']
    if root == None:
        root="."

    from os import path

    objects = LibrObject.get_objects(root)

    errors  = [ obj for _,obj in objects.items() if obj.parse_error != None ]

    if len(errors) > 0:
        print("Errors occured while parsing objects:",file=sys.stderr)
        for obj in errors:
            print("=> Error: In",obj.path+":",obj.parse_error,file=sys.stderr)
        raise Panic("Parse errors.")

    from ..database import DB, Book

    db = DB(db_dir)

    with db.session as session:
        Book.bulk_load(session, (obj for _,obj in objects.items()))
        session.commit()

# ** Sub-Command 'text-from-pdf' ----------------------------------------------|

@command("text-from-pdf", "output text from pdf")
def load_objects(gargs,args):
    """
    Sub-command 'libr text-from-pdf':

    Usage:
      libr text-from-pdf <file>

    The following global obtions apply:
    
      (TBD)

    Note, that the text output with this method is not very usable,
    though, since spaces are often missing and footer and header
    format mix wildly with content. This command is mostly for
    debugging, to see which information is, e.g. processed by
    isbns-from-pdf.
    """

    from ..pdfs import PdfFile
    
    filepath = args['<file>']

    with PdfFile(filepath) as book:
        for line in book.get_lines():
            print(line)

# ** Sub-Command 'isbns-from-pdf' ----------------------------------------------|

@command("isbns-from-pdf", "isbns from pdf")
def load_objects(gargs,args):
    """
    Sub-command 'libr isbns-from-pdf':

    Usage:
      libr isbns-from-pdf <file>

    The following global obtions apply:
    
      (TBD)
    """

    from ..pdfs import PdfFile
    
    filepath = args['<file>']

    with PdfFile(filepath) as book:
        for isbn in book.get_isbns():
            print(isbn)
    
            
# ** Sub-Command 'metadata-from-pdf' ----------------------------------------------|

@command("metadata-from-pdf", "metadata from pdf")
def load_objects(gargs,args):
    """
    Sub-command 'libr metadata-from-pdf':

    Usage:
      libr metadata-from-pdf <file>

    The following global obtions apply:
    
      (TBD)

    Note, that some metadata is simply unusable. Scraping authors and
    title from pdfs, on the other side, requires guessing the
    publisher (e.g. from the isbn) and then it's often more reliable
    to scrape the information from the publisher website. Of the
    information is there, it's often simply wrong.
    """

    from ..pdfs import PdfFile
    
    filepath = args['<file>']

    with PdfFile(filepath) as book:
        info = book.info.as_dict
        for key, value in info.items():
            print("{KEY:10}\t{VALUE}".format(
                KEY   = key,
                VALUE = value
            ))
        
            
# ** main()  ------------------------------------------------------------------|

def main(name=sys.argv[0], argv=sys.argv[1:]):
    gargs    = docopt(__doc__, version=version, options_first=True, argv=argv)
    cmd      = gargs['<command>']
    cmd_args = gargs['<args>']

    del gargs['<command>']
    del gargs['<args>']

    try:
        command.dispatch(cmd,gargs,cmd_args)
    except Panic as p:
        from os import path
        print(path.basename(name)+": *** Error ***", p, file=sys.stderr)

    
