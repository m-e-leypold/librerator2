# Rename at some later time XXX

from behave import *

@given(u'a file tree with N={N} media bundles')
def step_impl(context,N):
    context.n_objects = N
    # XXX assert on N and fixture size
    # XXX actually build the file object tree
    # raise NotImplementedError(u'STEP: Given a file tree with N media bundles')

@when(u'running sub-command find-objects')
def step_impl(context):
    # raise NotImplementedError(u'STEP: When running sub-command find-objects')
    pass

@when(u'redirecting to a file')
def step_impl(context):
    # raise NotImplementedError(u'STEP: When redirecting to a file')
    pass


@then(u'a list of N tab-separated object ids and paths is written into the file')
def step_impl(context):
    # raise NotImplementedError(u'STEP: Then a list of tab-separated object ids and paths is written into the file')
    pass
