Feature: Example behave test

  # This is only an example scenario wich currently doesn't test anything
  # 
  Scenario: Our first example
    Given a file tree with N=5 media bundles
    When  redirecting to a file
    And   running sub-command find-objects
    Then  a list of N tab-separated object ids and paths is written into the file
