# Librerator

*Librerator* is a program to manage a personal digital
library. Currently it is very much work in progress.

The primary code repository for *Librerator* is currently at 
https://gitlab.com/m-e-leypold/librerator2.

For the moment, if you want to contact me, e.g. because you want to
contribute or you have questions, create an issue in the gitlabs issu
tracking.

The source code is distributed and redistributable under the GPL 3.0
or later, see [LICENSE](LICENSE).

