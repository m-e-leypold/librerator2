#!/bin/sh

# *  Librerator 2 -- A library management program.  ---------------------------|
#    Copyright (C) 2019  M E Leypold
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# *  Setup.sh: Setting up the development directory  --------------------------|

rm -rf .pyenv3
python3 -mvenv .pyenv3
. .pyenv3/bin/activate
pip install --upgrade pip
pip install docopt
pip install pep8
pip install flake8
pip install pyflakes
pip install pylint
pip install mypy           # TBD: Actually apply this
pip install typeguard      # TBD: Actually apply this

pip install sqlalchemy
pip install pyyaml
pip install wheel
pip install isbnlib isbntools
pip install pdftotext
pip install loguru
pip install behave


